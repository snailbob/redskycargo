<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends CI_Controller {


	public function __construct(){

		parent::__construct();

		$domain = $this->common->the_domain();
        $arr = array(
            'demo',
            'ccua',
            'agile'
        );
        if(in_array($domain, $arr) && $this->uri->segment(1) == 'webmanager'){
			$url = 'https://ccua.redskycargo.com/web/webmanager';
            if($domain == 'agile'){
				$url = 'https://agile.redskycargo.com/site/webmanager';
            }
            if($domain == 'demo'){
				$url = 'https://demo.redskycargo.com/d/webmanager';
            }

            redirect($url);
        }
        else if($this->common->the_domain()  != ''){
            $url = 'https://redskycargo.com/webmanager';
            redirect($url);
            // $this->config->set_item('base_url',$this->common->base_url());
        }
	}


	/*-------------------------------------
		index
	-------------------------------------*/
	public function index(){
		//redirect('admin/dashboard/users');

//		echo $_SESSION['logged_admin'];
//		echo $this->session->userdata('logged_admin_email');
//		echo json_encode($this->session->all_userdata());
//
//		return false;

		redirect('webmanager/agency/manage');

		//seekers
		// $insurances = $this->master->getRecords('bought_insurance','', '', array('status'=>'DESC'));
		//
		// $data = array(
		// 	'title'=>'Dashboard',
		// 	'middle_content'=>'dashboard',
		// 	'insurances'=>$insurances
		//
		// );
		//
		// $this->load->view('admin/admin-view',$data);

	}

	/*-------------------------------------
		referrals
	-------------------------------------*/
	public function referrals(){
		//redirect('admin/dashboard/users');

		//seekers
		$insurances = $this->master->getRecords('bought_insurance', array('status !='=>'P'), '', array('status'=>'DESC'));

		$data = array(
			'title'=>'Policy Referrals',
			'middle_content'=>'manage-referrals',
			'insurances'=>$insurances

		);

		$this->load->view('admin/admin-view',$data);

	}

	/*-------------------------------------
		transactions
	-------------------------------------*/
	public function transactions(){
		//redirect('admin/dashboard/users');

		//seekers
		$insurances = $this->master->getRecords('bought_insurance', array('status'=>'P'), '', array('status'=>'DESC'));

		$data = array(
			'title'=>'Transactions',
			'middle_content'=>'manage-transactions',
			'insurances'=>$insurances

		);

		$this->load->view('admin/admin-view',$data);

	}

	public function accept_request(){
		//premium=1.57&id=21&comment=sdff
		$premium = $_POST['premium'];
		$id = $_POST['id'];
		$comment = $_POST['comment'];

		$insurances = $this->master->getRecords('bought_insurance', array('id'=>$id));

		$details = unserialize($insurances[0]['details']);
		$details['premium'] = round($premium, 2);
		$arr = array(
			'comment'=>$comment,
			'details'=>serialize($details),
			'status'=>'A'
		);

		$this->master->updateRecord('bought_insurance', $arr, array('id'=>$id));

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for agency
		$info_arr = array(
			'to'=>$insurances[0]['email'], //$adminemail,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'referral-acceptance-to-user',
			'emailer_file_name'=>'referral-acceptance-to-user',
		);


		$other_info = array(
			'password'=>'',
			'view'=>'referral-acceptance-to-user',
			'emailer_file_name'=>'referral-acceptance-to-user',
			'name'=>'',//$first.' '.$last,
			'agency_name'=>'Admin',
			'user_name'=>$adminemail,
			'user_email'=>$adminemail,
			'link'=>'landing/purchasepolicy/'.$id.'/'.md5($insurances[0]['email'])
		);

		$data['info_arr1'] = $info_arr;
		$data['other_info1'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);
		$arr['data'] = $data;

		$this->session->set_flashdata('ok', ' Referral request successfully accepted.');
		redirect(base_url().'webmanager/dashboard/referrals');
		//echo json_encode($arr);
	}

	public function send_certificate(){
		$id = $_POST['id'];
		$insurances = $this->master->getRecords('bought_insurance', array('id'=>$id));

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for agency
		$info_arr = array(
			'to'=>$insurances[0]['email'], //$adminemail,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'send-certifiicate-to-user',
			'emailer_file_name'=>'send-certifiicate-to-user',
		);

		$other_info = array(
			'password'=>'',
			'view'=>'send-certifiicate-to-user',
			'emailer_file_name'=>'send-certifiicate-to-user',
			'name'=>'',//$first.' '.$last,
			'agency_name'=>'Admin',
			'user_name'=>$adminemail,
			'user_email'=>$adminemail,
			'link'=>'landing/download/'.$id, //landing/purchasepolicy/'.$id.'/'.md5($insurances[0]['email'])
		);

		$data['info_arr1'] = $info_arr;
		$data['other_info1'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);

		echo json_encode($other_info);
	}

	public function reject_request(){
		$id = $this->uri->segment('4');
		$insurances = $this->master->getRecords('bought_insurance', array('id'=>$id));

		$arr = array(
			'status'=>'R'
		);
		$this->master->updateRecord('bought_insurance', $arr, array('id'=>$id));

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for agency
		$info_arr = array(
			'to'=>$insurances[0]['email'], //$adminemail,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'referral-rejected-to-user',
			'emailer_file_name'=>'referral-rejected-to-user',
		);


		$other_info = array(
			'password'=>'',
			'view'=>'referral-rejected-to-user',
			'emailer_file_name'=>'referral-rejected-to-user',
			'name'=>'',//$first.' '.$last,
			'agency_name'=>'Admin',
			'user_name'=>$adminemail,
			'user_email'=>$adminemail,
			'link'=>'dashboard', //landing/purchasepolicy/'.$id.'/'.md5($insurances[0]['email'])
		);

		$data['info_arr1'] = $info_arr;
		$data['other_info1'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);

		$this->session->set_flashdata('ok', ' Request successfully rejected.');
		redirect(base_url().'webmanager/dashboard');
	}

	/*-----------------------------------------
		Admin login page
	-----------------------------------------*/
	public function login(){

		$error="";

		//if(isset($_SESSION['logged_admin']))
		if($this->session->userdata('logged_admin') != ''){

			redirect(base_url().'webmanager/dashboard');
		}

		else{

			if(isset($_POST['btn_admin_login'])){

				$email = $this->input->post('user_name',true);
				$pw = $this->input->post('pass_word',true);
				$chk_arr = array(
					'name'=>$email,
					'password'=>md5($pw)
				);
				$row = $this->master->getRecords('admin',$chk_arr);
				if(count($row) > 0){

					$_SESSION['logged_admin'] = $row[0]['name'];
					$_SESSION['logged_admin_email'] = $row[0]['email'];

					$this->session->set_userdata('logged_admin', $row[0]['name']);
					$this->session->set_userdata('logged_admin_email', $row[0]['email']);


					$log_activity = array(
						'name'=>$row[0]['name'].' webmanager login',
						'type'=>'login',
						'details'=>serialize($row[0])
					);
					$this->master->insertRecord('activity_log', $log_activity);


//					echo $this->session->userdata('logged_admin');
//					echo $this->session->userdata('logged_admin_email');
//					return false;
					redirect(base_url().'webmanager/dashboard');

				}

				else{
					$error = "Invalid Credentials";
				}


			}


			$data = array(
				'page_title'=>'Admin Login',
				'error'=>$error
			);

			$this->load->view('admin/index',$data);


		}

	}







	/*----------------------------------------------
				Admin Forgot password
	-----------------------------------------------*/

	public function forgotpassword(){

		$whr = array('id'=>'2');

		$email_id = $this->master->getRecords('admin',$whr,'*');

		$data = array(
			'page_title'=>"Forget Password",
			'middle_content'=>'forget-password',
			'success'=>'',
			'error'=>''
		);


		if(isset($_POST['btn_forget'])){


			$email = $this->input->post('user_name',true);

			if($email == $email_id[0]['email']){

				$info_arr = array(
					'from'=>$email_id[0]['email'],
					'to'=>$email_id[0]['email'],
					'subject'=>'Password Recovery',
					'view'=>'forget-password-mail-to-admin'
				);

				$other_info = array(
					'password'=>'Darknite1',//$email_id[0]['pass_word'],
					'email'=>$email
				);

				$this->email_sending->sendmail($info_arr,$other_info);
				$data['success']='Password mail send successfully.';
			}

			else{
				$data['error'] = 'Email Address is invalid.';
			}

		}

		$this->load->view('admin/forget-password',$data);

	}


	/*-----------------------------
			Admin logout
	-----------------------------*/

	public function logout(){

		//unset($_SESSION['logged_admin']);

//		$this->session->unset_userdata('logged_admin');
//		$this->session->unset_userdata('logged_admin_email');
//
//		redirect('webmanager/dashboard/login');

		$user_data = $this->session->all_userdata();

		$log_activity = array(
			'name'=>$user_data['logged_admin'].' user logout',
			'type'=>'logout',
			'details'=>serialize($user_data)
		);
		$this->master->insertRecord('activity_log', $log_activity);


		foreach ($user_data as $key => $value) {
			$this->session->unset_userdata($key);
		}

		redirect(base_url());


	}



}
