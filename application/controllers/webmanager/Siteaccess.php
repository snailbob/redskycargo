<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Siteaccess extends CI_Controller 

{

	public function __construct(){

        parent::__construct();
    }

    public function manage(){

		$data = array(
			'middle_content'=>'manage-siteaccess',
			'title'=>'Webmanager Access',
		);	
		
		$this->load->view('admin/admin-view',$data);

    }
}