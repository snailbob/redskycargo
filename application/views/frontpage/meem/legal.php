<div id="row-drivers" class="content-holder content-vert-padding">
   <div class="container clearfix">
      <div class="content-side-padding">
         <h3 class="title-content text-center">Terms of Use</h3>
      </div>
      <div class="row">
         <div class="col-sm-3 col-lg-3 still_menu">
            <ul class="mainmenu gotquestion_menu">
               <li><a href="#answer1" data-target="answer1" data-hide="yes" data-title="Terms of Use" class="active">Terms of Use <i class="fa fa-angle-double-right pull-right"></i></a></li>
               <li><a href="#answer2" data-target="answer2" data-hide="yes" data-title="Privacy Policy">Privacy Policy  <i class="fa fa-angle-double-right pull-right hidden"></i></a></li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-3 answer_container">
            <div id="answer1">
               <?php echo $legals[0]['terms'] ?>
            </div>
            <div id="answer2" class="hidden">
               <?php echo $legals[0]['privacy'] ?>
            </div>
         </div>
      </div>
   </div>
</div>
