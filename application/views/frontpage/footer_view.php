	<?php if($this->uri->segment(1) != '' && $this->session->userdata('id') != '') { ?>
        </div><!--/.row-->
    </div>
    <!-- /.container -->
    <?php }?>

    <div id="footer" class="<?php echo ($this->uri->segment(1) == 'landing') ? 'hidden' : '' ?>">
    	<div class="container">
        	<div class="row hidden">
            <div class="col-md-12 col-md-offset-1 addLeftPadding">


            	<div class="col-md-5 col-xs-12">
                	<h3>Recent Blogs</h3>
                </div>

            	<div class="col-md-2 col-xs-12">
                	<h3>Company</h3>
                </div>
            	<div class="col-md-5 col-xs-12">
                	<h3>Location</h3>
                </div>
            </div><!--offset2-->
            </div> <!-- /row -->

            <div id="footer-bottom" class="row">
            	<?php /*?><a id="footer-linkedin" target="_blank" href="http://www.linkedin.com/company/crisis-flo" class="footer-icon"></a>
            	<a id="footer-twitter" target="_blank" href="https://twitter.com/CrisisFLOOnline" class="footer-icon"></a>
            	<a id="footer-facebook" target="_blank" href="https://www.facebook.com/crisisflo" class="footer-icon" style="display: none;"></a>
            	<!--<a id="footer-youtube" target="_blank" href="http://www.youtube.com/" class="footer-icon"></a>
            	<a id="footer-gplus" target="_blank" href="https://plus.google.com/" class="footer-icon"></a>
            	<a id="footer-rss" target="_blank" href="<?php echo base_url()?>" class="footer-icon"></a>--><?php */?>

                <div id="copyright">
                	<ul>
                    	<li class="text-muted">&copy; 2017 Your Company Pty Ltd</li>
                    </ul>
                	<ul>
                    	<li><a href="#" class="terms_btn">Terms of Use</a></li>
                    	<li><a href="#" class="privacy_btn">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div> <!-- /container -->
    </div> <!-- /footer -->


    <div id="top-button" class="animated fadeInUp" data-0="display: none" data-100="display: block">
    	<img src="<?php echo base_url()?>assets/frontpage/corporate/images/top-button.png" />
    </div>

	</div>





    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Terms of Use</h1>
                            <hr class="star-primary">

                            <div class="text_content text-left"></div>

                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->


    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <h1>Forgot Password</h1>



                            <div class="omb_login">

                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                        <hr class="star-primary">
                                        <p class="text-center">Enter your email below and we will send you instructions on how to reset your password</p>

                                        <form class="omb_loginForm" id="forgot_form">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-primary btn-block" type="submit">Send</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                            <h1>Customer Sign Up</h1>
                            <div class="omb_login">

                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">

                                        <hr class="star-primary">
                                        <p class="text-center">Already have an account? <a href="#" class="login_btn">Log In</a></p>

                                    	<div class="alert alert-success" style="display: none;">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text"></span>
                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                        <form class="omb_loginForm" id="signup_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">
                                            <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">

                                            <input type="hidden" class="form-control" name="google_img">
                                            <input type="hidden" class="form-control" name="fb_img">
                                            <input type="hidden" class="form-control" id="social_type" name="social_type">
                                            <input type="hidden" class="form-control" id="social_site" name="social_site">


                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-signup">
                                                  <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                                    Sign up as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>

                                                <input type="hidden" name="utype" value="Customer" />
                                            </div>

                                            <div class="form-group studio_only hidden">


                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                    <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                                </div>
                                            </div>


                                            <div class="row notfor_studio">
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                            <input type="text" class="form-control" name="first_name" placeholder="First Name">
                                                        </div>
                                                    </div>

                                                </div>

                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>



                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox" name="terms"> I have read and understood <a href="#" class="terms_btn2">terms of use</a> &amp; <a href="#" class="privacy_btn2">conditions</a>
                                                </label>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-primary btn-block" type="submit">Sign Up</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <h1>Log In</h1>

                            <div class="omb_login">
                                <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <hr class="star-primary">
                                        <p class="text-center">Don't have an account? <a href="#" class="signup_btn">Sign Up</a></p>
                                    	<div class="alert <?php if($this->session->flashdata('info')) { echo 'alert-success'; } else { echo 'hidden'; }?>">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').addClass('hidden');" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text">
                                            	<?php echo $this->session->flashdata('info')?>
                                            </span>
                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <form class="omb_loginForm" id="login_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">
                                            <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">
                                            <input type="hidden" class="form-control" name="from_buyform" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; }?>">

                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-login">
                                                  <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right" style="margin-right: 10px;"></i>
                                                    Log In as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>

                                                <input type="hidden" name="utype" value="Customer" />
                                            </div>

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="form-group hidden">
                                            	<label>Log In as</label>
                                                <input type="checkbox" name="usertype" checked>
                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-primary btn-block" type="submit">Log In</button>
                                            </div>


                                        </form>

                                        <p class="omb_forgotPwd">
                                            <a href="#" class="forgot_btn">Forgot password?</a>
                                        </p>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->





  <ul class="map-details hidden">
    <li>Location: <span data-geo="location"></span></li>
    <li>Route: <span data-geo="route"></span></li>
    <li>Street Number: <span data-geo="street_number"></span></li>
    <li>Postal Code: <span data-geo="postal_code"></span></li>
    <li>Locality: <span data-geo="locality"></span></li>
    <li>Country Code: <span data-geo="country_short"></span></li>
    <li>State: <span data-geo="administrative_area_level_1"></span></li>
  </ul>





    <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>

    <!-- Plugin JavaScript -->
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
    <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/pwstrength/pwstrength-bootstrap.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/silviomoreto-bootstrap-select/js/bootstrap-select.min.js"></script>

	<?php if((($this->uri->segment(2) == 'profile' && $this->session->userdata('stripe_id') == '') || ($this->uri->segment(1) == 'buy' || $this->uri->segment(1) == '')) && $this->session->userdata('customer_type') != 'N') { ?>
    <!-- Plugin JavaScript -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/validate/jquery.payment.min.js"></script>
    <!-- If you're using Stripe for payments -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <?php } ?>

	<?php if($this->uri->segment(1) == 'settings' && $this->uri->segment(2) == 'profile') { ?>
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/cropper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script>
	<?php } ?>

    <!-- Custom Theme JavaScript -->
   <?php /*?> <script src="<?php echo base_url() ?>assets/js/creative.js"></script><?php */?>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>



</body>
</html>
