<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3>Manage Policy Documents</h3>
            <ol class="breadcrumb">
               <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
               <li class="active">Manage Policy Documents</li>
            </ol>
         </div>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
   <!-- Form AREA -->
   <div class="row">
      <div class="col-lg-12">
         <?php if($this->session->flashdata('success')!=""){ ?>
         <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>
         </div>
         <?php } if($this->session->flashdata('error')!=""){ ?>
         <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?>
         </div>
         <?php } ?>
      </div>
      <div class="col-lg-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <div  class="panel-title">
                  <h4><?php echo $title; ?></h4>
               </div>
            </div>
            <div class="panel-body">
               <p class="text-center">
                  <a href="#" class="btn btn-primary" onclick="$('#myfile').click()">Upload Policy Document</a>
               </p>
               <div id="progress" class="progress-profile" style="display: none;">
                  <div id="bar" class="bar-profile"></div>
                  <div id="percent" class="percent-profile">0%</div >
               </div>
               <p class="lead text-center policydoc">
                  <?php
                     if(count($policy_docs) > 0) {
                     	echo '<a href="'.base_url().'uploads/policyd/'.$policy_docs[0]['name'].'" target="_blank" class=""><i class="fa fa-file-pdf-o"></i> '.substr($policy_docs[0]['name'], 13).'</a> <small><i class="fa fa-times-circle text-muted delete_policy_btn"></i></small>';

                     }
                     ?>
               </p>
            </div>
         </div>
      </div>
      <div class="row hidden">
         <form id="uploadPolicyForm" action="<?php echo base_url().'settings/policydoc' ?>" method="post" enctype="multipart/form-data">
            <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$(this).closest('form').submit();">
            <input type="hidden" id="avatar_name" value="">
            <input type="submit" class="hidden" value="Ajax File Upload">
         </form>
      </div>
   </div>
   <!--.row-->
</div>
<!-- Modal for faqs -->
<div class="modal fade bottom" id="faqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <p class="lead">Policy Document</p>
            <form id="faq_form" role="form">
               <fieldset>
                  <input type="hidden" class="form-control" name="faq_id" id="faq_id" value=""/>
                  <div class="form-group">
                     <textarea class="form-control" placeholder="Question" name="question" id="question"></textarea>
                  </div>
                  <div class="form-group">
                     <textarea class="form-control" placeholder="Answer" name="answer" id="answer" rows="5"></textarea>
                  </div>
                  <div class="form-group">
                     <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
               </fieldset>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- /.modal -->
