<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
   <div class="row">
      <div class="col-lg-12">
         <div class="page-title">
            <h3> Transit Insurance Admin Dashboard</h3>
            <ol class="breadcrumb">
               <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
            </ol>
         </div>
         <?php if ($this->session->flashdata('ok') != ''){ ?>
         <div class="alert alert-success fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('ok'); ?>
         </div>
         <?php } ?>
         <?php if ($this->session->flashdata('error') != ''){ ?>
         <div class="alert alert-danger fade in">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <?php echo $this->session->flashdata('error'); ?>
         </div>
         <?php } ?>
         <div class="panel panel-default">
            <div class="panel-heading">
               <div class="panel-title">
                  <h4><?php echo $title; ?></h4>
               </div>
            </div>
            <div class="panel-body">
            </div>
         </div>
         <!--panel-->
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <!-- end PAGE TITLE AREA -->
</div>
<!--end of main_content-->
<!-- Modal -->
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Review Policy</h4>
         </div>
         <form id="admin_review_form">
            <div class="modal-body">
               <div class="form-group">
                  <label>Adjust Quoted Premium</label>
                  <input type="text" class="form-control" name="premium" value="" />
                  <input type="hidden" class="form-control" name="id" value="" />
               </div>
               <div class="form-group">
                  <label>Comments</label>
                  <textarea name="comment" class="form-control"></textarea>
               </div>
            </div>
            <div class="modal-footer">
               <a href="#" class="btn btn-danger pull-left reject_btn">Reject</a>
               <button type="submit" class="btn btn-primary">Accept</button>
            </div>
         </form>
      </div>
   </div>
</div>
