<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>DancePass</title>
<style type="text/css">
a:hover { color: #09F !important; text-decoration: underline !important; }
</style>
</head>
<body style="margin: 0px; background-color: #ffffff; background-image: url(<?php echo base_url() ?>assets/img/header.jpg); background-repeat: repeat;" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" bgcolor="#ffffff">
<!--100% body table-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <!--top links-->
            <table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" height="20"></td>
                </tr>
                <tr>
                    <td height="80" valign="top">
                        <?php /*?><h1 style="font-size: 58px; font-family: Georgia, 'Times New Roman', Times, serif; margin: 0px; padding: 0px; color: #5e5e5e; font-weight: normal;">DancePass.com</h1><?php */?>
                        <img src="<?php echo base_url().'assets/img/logo.png' ?>" width="181" height="22" alt=""/>
                    </td>
                </tr>
            </table>
            <!--/top links-->


            <!--email content-->
            <table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="40"></td>
                </tr>
                <tr>
                    <td style="background-color: #f1f1f1; border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; -khtml-border-radius: 6px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="40">
                            <tr>
                                <td valign="top" width="100%">
                                <?php
									$emailer = $this->master->getRecords('outgoing_emails',array('file_name'=>$emailer_file_name));
								?>
                                    <p style="color: #5e5e5e; font-family:Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; margin-top: 0; padding: 0px; font-size: 16px; font-weight: bold;"><?php if($name !='') { echo 'Hey there, '.$name.'!'; } else { echo 'Hey there!';} ?></p>
                                    <p style="color: #5e5e5e; font-family:Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; margin-top: 0; padding: 0px; font-size: 16px; font-weight: normal; text-align: justify"><br>
         
<?php
	$click = '<a href="'.base_url().'" target="_blank">click here</a>';
	$clicking = '<a href="'.base_url().'" target="_blank">clicking here</a>';
	
	$healthy = array("[click_here]", "[clicking_here]","[password]","[user_email]","[user_name]");
	$yummy   = array($click, $clicking, $password, $user_email, $user_name); //this->session->userdata('name')

	$newphrase = str_replace($healthy, $yummy, $emailer[0]['content']);

	echo $newphrase;
?>         


<br><br></p>

<p style="text-align: center; color: #ccc;">
	<span >You may copy &amp; paste this link if button does not work.<br><?php echo base_url().$link ?></span>
<br><br></p>

                        <!--button-->
                        <a href="<?php echo base_url().$link ?>">
                        <table width="100%" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td style="border-radius: 300px; -moz-border-radius: 300px; -webkit-border-radius: 300px; -khtml-border-radius: 300px; font-size: 12px; font-weight: 700; font-family: Georgia, 'Times New Roman', Times, serif; color: #fff; margin: 0; padding: 0; text-tranform: uppercase; text-align: center;" height="44" bgcolor="#f05f40"><span style="text-decoration: none;font-weight: bold; color: #fff; text-transform: uppercase"><?php echo $emailer[0]['button_text'] ?></span></td>
                            </tr>
                        </table>
                        </a>
                        <!--/button-->

                                    <p style="color: #5e5e5e; font-family:Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; margin-top: 0; padding: 0px; font-size: 16px; font-weight: normal; text-align: justify"><br><br>Regards,<br>

<span style="color: #5e5e5e; font-family:Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; margin-top: 0; padding: 0px; font-size: 16px; font-weight: bold;">DancePass</span>

</p>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="40">&nbsp;</td>
                </tr>
            </table>
            <!--/email content-->
            <!--footer-->
            <table style="background-color: #f1f1f1; border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; -khtml-border-radius: 6px;" width="620" border="0" align="center" cellpadding="20" cellspacing="0">
                <tr>
                    <td valign="top">
                        <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; margin-top: 0; padding: 0px; color: #5e5e5e; font-weight: bold; font-style:italic;">Like us on Facebook</p>
                        <!--button-->
                        <a href="https://www.facebook.com/dancepassdotcom"><table width="100" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td style="border-radius: 300px; -moz-border-radius: 300px; -webkit-border-radius: 300px; -khtml-border-radius: 300px; font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0; padding: 0; text-align: center;" height="44" bgcolor="#f05f40"><forwardtoafriend style="text-decoration: none; color: #fff;">Facebook</forwardtoafriend></td>
                            </tr>
                        </table></a>
                        <!--/button-->

                    </td>
                    <td valign="top">
                        <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; margin-top: 0; padding: 0px; color: #5e5e5e; font-weight: bold; font-style:italic;">Follow us on Twitter</p>
                        <!--button-->
                        <a href="https://twitter.com/DancePass_dotcom"><table width="100" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td style="border-radius: 300px; -moz-border-radius: 300px; -webkit-border-radius: 300px; -khtml-border-radius: 300px; font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0; padding: 0; text-align: center;" height="44" bgcolor="#f05f40"><forwardtoafriend style="text-decoration: none; color: #fff;">Twitter</forwardtoafriend></td>
                            </tr>
                        </table></a>
                        <!--/button-->
                    </td>    
                    <td valign="top">
                        <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; margin-top: 0; padding: 0px; color: #5e5e5e; font-weight: bold; font-style:italic;">Contact Us</p>
                        <!--button-->
                        <a href="mailto:hello@dancepass.com"><table width="100" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td style="border-radius: 300px; -moz-border-radius: 300px; -webkit-border-radius: 300px; -khtml-border-radius: 300px; font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; color: #333; margin: 0; padding: 0; text-align: center;" height="44" bgcolor="#f05f40"><forwardtoafriend style="text-decoration: none; color: #fff;">Email</forwardtoafriend></td>
                            </tr>
                        </table></a>
                        <!--/button-->
                    </td>

                                    
                </tr>
                <tr>
                	<td colspan="3" valign="top">
                    <hr>
                	<p style="font-family:Georgia, 'Times New Roman', Times, serif; color: #999">Copyright &copy; 2015. DancePass.com.</p>
                    </td>
                </tr>
            </table>
            <!--/footer-->
        </td>
    </tr>
    <tr>
        <td height="40">&nbsp;</td>
    </tr>
</table>
<!--/100% body table-->
</body>
</html>