
<section class="section section-simple">
	<div class="container">
    	<div class="row">
        
        	<div class="col-md-12">
            	<?php if ($this->session->flashdata('ok') != ''){ ?>
                <div class="alert alert-success fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('ok'); ?>
                </div>
                <?php } ?>
            
            	<?php if ($this->session->flashdata('error') != ''){ ?>
                <div class="alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
            
       
       
            	<div class="panel panel-default">
                
                	<div class="panel-heading  bg-shoes">
                        <h2 class="my-panel-title"><?php echo $title; ?></h2>
                    </div>
                    <div class="panel-body">
                    	<div class="row gutter-md">
                        	<div class="col-sm-4">
                            	<div class="well well-warning text-center">
                                	<h4 class="panel-title">Base Rate - <strong class="base_rate">$<?php echo $agency_info[0]['base_rate'] ? $agency_info[0]['base_rate'] : 0 ?></strong></h4><br />

                                    <a href="#baseRateModal" data-toggle="modal" data-controls-modal="baseRateModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div>
                        
                        	<div class="col-sm-4">
                            	<div class="well well-info text-center">
                                	<h4 class="panel-title">Minimum Premium - <strong class="minimum_premium">$<?php echo $agency_info[0]['minimum_premium'] ? $agency_info[0]['minimum_premium'] : 0 ?></strong></h4><br />

                                    <a href="#minimumPremiumModal" data-toggle="modal" data-controls-modal="minimumPremiumModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div>
                        
                        	<div class="col-sm-4">
                            	<div class="well well-blue text-center">
                                	<h4 class="panel-title">Cargo Category</h4><br />

                                    <a href="#cargoCategoryModal" data-toggle="modal" data-controls-modal="cargoCategoryModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div>
                        
                        	<div class="col-sm-4 col-sm-offset-2">
                            	<div class="well well-gold text-center">
                                	<h4 class="panel-title">Transportation</h4><br />

                                    <a href="#transportationModal" data-toggle="modal" data-controls-modal="transportationModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div>
                        
                        	<div class="col-sm-4">
                            	<div class="well well-maroon text-center">
                                	<h4 class="panel-title">Country</h4><br />

                                    <a href="#countryModal" data-toggle="modal" data-controls-modal="countryModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div>
                        
                        </div>
                    	
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="baseRateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h2>Base Rate</h2>
                            <hr class="star-primary">
                            
                            <form class="form-rate form-input-lg">
                            	<div class="form-group">
                                	<label>Base Rate</label>
                                    <input type="text" class="form-control" name="rate" value="<?php echo $agency_info[0]['base_rate']?>" />
                                    <input type="hidden" name="type" value="base_rate" />
                                </div>
                            	<div class="form-group">
                                	<button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>
                     
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
    
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="minimumPremiumModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h2>Minimum Premium</h2>
                            <hr class="star-primary">
                            
                            <form class="form-rate form-input-lg">
                            	<div class="form-group">
                                	<label>Minimum Premium</label>
                                    <input type="text" class="form-control" name="rate" value="<?php echo $agency_info[0]['minimum_premium']?>" />
                                    <input type="hidden" name="type" value="minimum_premium" />
                                </div>
                            	<div class="form-group">
                                	<button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>
                     
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
    
                                
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="cargoCategoryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                        
                            <h2>Cargo Category</h2>
                            <hr class="star-primary">
                            
							<?php
                                if(count($cargo) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <th width="75%">Name</th>
                                        <th width="25%">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <?php foreach($cargo as $r=>$value){
                                        $acargo_price = 0;
                                        if(isset($cargo_prices[$value['id']])) {
                                            $acargo_price = $cargo_prices[$value['id']];
                                        }
                                    ?>
                                    
                                        <tr>
                                            <td><?php echo $value['name']; ?></td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->session->userdata('id') ?>" />
                                                    <input type="hidden" name="agency-price-type" value="cargo_prices" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    
                                    
                                    
                                </tbody>
                            </table>
                            <?php } ?>                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
                           
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="countryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                        
                            <h2>Countries</h2>
                            <hr class="star-primary">
                            
							<?php
                                if(count($countries) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <th width="75%">Name</th>
                                        <th width="25%">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <?php foreach($countries as $r=>$value){
                                        $acargo_price = 0;
                                        if(isset($country_prices[$value['country_id']])) {
                                            $acargo_price = $country_prices[$value['country_id']];
                                        }
                                    ?>
                                    
                                        <tr>
                                            <td><?php echo $value['short_name']; ?></td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $value['country_id'] ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['country_id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->session->userdata('id') ?>" />
                                                    <input type="hidden" name="agency-price-type" value="country_prices" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    
                                    
                                    
                                </tbody>
                            </table>
                            <?php } ?>                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
                     
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="transportationModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                        
                            <h2>Transportation</h2>
                            <hr class="star-primary">
                            
							<?php
                                if(count($cargo) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <th width="75%">Name</th>
                                        <th width="25%">Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <?php
									$count = 0;
									foreach($transportations as $r){
										$acargo_price = 0;
				                        if(isset($transportation_prices[$count])) {
                                            $acargo_price = $transportation_prices[$count];
                                        }
                                    ?>
                                    
                                        <tr>
                                            <td><?php echo $r; ?></td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $count ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $count; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->session->userdata('id') ?>" />
                                                    <input type="hidden" name="agency-price-type" value="transportation_prices" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php
										$count++;
									} ?>
                                    
                                    
                                    
                                </tbody>
                            </table>
                            <?php } ?>                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
