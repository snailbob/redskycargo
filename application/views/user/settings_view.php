
<section class="section section-simple">
	<div class="container">
    	<div class="row">
        

        	<div class="col-md-3 col-sm-4">
               <div class="row hidden">
                <form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
                     <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
                     <input type="hidden" id="avatar_name" value="">
                     <input type="submit" class="hidden" value="Ajax File Upload">
                 </form>                                                   
                </div>
    

                <a href="#" class="portfolio-box text-center" onclick="$('.myfile-profile').click(); return false;">
                    <img src="<?php echo $this->common->avatar($agency[0]['id']) ?>" class="img-thumbnail" alt="" width="100%">
                    <div class="portfolio-box-caption div-rounded">
                        <div class="portfolio-box-caption-content">
                            <div class="project-category text-faded">
                                
                            </div>
                            <div class="project-name">
                                Update Agency Logo
                            </div>
                        </div>
                    </div>
                </a>
                <p></p>

                <div id="progress" class="progress-profile" style="display: none;">
                    <div id="bar" class="bar-profile"></div>
                    <div id="percent" class="percent-profile">0%</div >
                </div>


            </div>

        	<div class="col-md-9 col-sm-8">
            	<div class="panel panel-default">
                
                	<div class="panel-body">
                        <h2 class="my-panel-title"><?php echo $title; ?></h2>

                    	
                        
                        <div>
                        
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li class="<?php if($this->uri->segment(3) == '') { echo 'active';}?>"><a href="#agency" aria-controls="agency" role="tab" data-toggle="tab"><?php echo ucfirst($this->session->userdata('type')); ?> Profile</a></li>
                            <li class="hidden <?php if($this->uri->segment(3) == 'connect') { echo 'active';}?>"><a href="#connect" aria-controls="connect" role="tab" data-toggle="tab">Connect</a></li>
                            <li class="<?php if($this->uri->segment(3) != '') { echo 'active';}?>"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Password</a></li>
                          </ul>
                        
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade <?php if($this->uri->segment(3) == '') { echo 'in active';}?>" id="agency">
                                <p></p>
                                
                                <form class="" id="agency_profile_form">
                                    
                                    <?php foreach($the_fields as $r){ ?>
                                    <div class="form-group">
                                        <label><?php echo ucfirst(str_replace('_', ' ', $r)); ?></label>
                                        <input type="text" name="<?php echo 'agency_'.$r ?>" class="form-control" value="<?php echo $agency[0][$r] ?>" placeholder="<?php echo ucfirst(str_replace('_', ' ', $r)); ?>"/>
                                    </div>
                                        
                                    <?php } ?>
                                    
                                    <div class="form-group">
                                        <label>Location</label>
                                        <input type="text" name="address" class="form-control user_geolocation" value="<?php echo $agency[0]['address'] ?>" placeholder="Location"/>
                                        <input type="hidden" name="lat" value="<?php echo $this->session->userdata('lat'); ?>"/>
                                        <input type="hidden" name="long" value="<?php echo $this->session->userdata('lng'); ?>"/>
                                        <input type="hidden" name="agency_id" value="<?php echo $agency[0]['id']?>"/>
                                    </div>
                                        
                                    
                                    <div class="row">
                                    
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label class="control-label">Country Calling Code</label>
                                                <select class="form-control country_code_dd" name="country_code">
                                                    <option value="" data-code="">Select..</option>
                                                    <?php
                                                        foreach($countries as $r=>$mc){
                                                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
															
															if($agency[0]['country_id'] == $mc['country_id']){
																echo ' selected="selected"';
															}
															
															echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div class="col-sm-7">
                                            <div class="form-group mobile_input">
                                                <label class="control-label">Mobile Number</label>
                                                <div class="input-group">
                                                  <span class="input-group-addon addon-shortcode">+</span>
                                                  <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" value="<?php echo $agency[0]['calling_digits']?>">
                                                </div>
                                                <input type="hidden" name="country_short" value=""/>
                                            </div>
                                        </div>
                                    
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Website</label>
                                        <input class="form-control" name="website" type="text" placeholder="http://example.com" value="<?php echo $agency[0]['website']?>"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Minimum Premium</label>
                                        <input class="form-control" name="minimum_premium" type="number" placeholder="" value="<?php echo $agency[0]['minimum_premium']?>"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Background</label>
                                        <textarea name="background" class="form-control" placeholder="Background"><?php echo $agency[0]['background']?></textarea>
                                    </div>
                                    
                                    <div class="form-group hidden">
                                        <label>Facilities</label>
                                        <textarea name="facilities" class="form-control" placeholder="Facilities"><?php echo $agency[0]['facilities']?></textarea>
                                    </div>
                                    
                                                                            
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a href="#" class="btn btn-default"><i class="fa fa-eye"></i> Preview</a>
                                    </div>
                                </form>
                            
                            </div>
                            <div role="tabpanel" class="tab-pane fade <?php if($this->uri->segment(3) == 'connect') { echo 'in active';}?>" id="connect">
                                <p></p>
                            	googogle
                            </div>
                            
                          
                            <div role="tabpanel" class="tab-pane fade <?php if($this->uri->segment(3) == 'pw') { echo 'in active';}?>" id="password">
                                <p></p>
                                <form class="" id="agency_password_form">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="pw" id="pw" class="form-control" />
                                        <input type="hidden" name="id" class="form-control" value="<?php echo $agency[0]['id'] ?>" />
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="pw2" class="form-control" />
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                        
                            </div>
                          </div>
                        
                        </div>                            
                        
                    </div>
                                        
                    

                </div>
            </div>
        </div>
    </div>
</section>





<!-- Modal -->
<div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<p class="lead">Crop</p>
        
        <div class="bootstrap-modal-cropper"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>
	
        

      </div>
      <div class="modal-footer">
        <a class="btn btn-primary sav_crop">Save changes</a>
      </div>
    </div>
  </div>
</div>