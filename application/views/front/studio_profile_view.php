    <section class="bg-primary" style="margin-top: 50px;">
        <div class="container">
        	<div class="row md-gutter">
                
                <div class="col-sm-6 col-md-3 col-md-offset-2" style="margin-top: 9px; padding-bottom: 15px;">

                        <img src="<?php echo $this->common->avatar($_GET['studio_id'],'studio')?>" class="img-rounded img-thumbnail img-opacity-effect wow fadeInLeft" alt="" width="100%">

                </div>
                
                
                <div class="col-sm-6 col-md-5 wow fadeInDownBig">
                    <p class="studio-details"><strong><?php echo $title; ?></strong><br />
                        <span class="">
                            
                            <span class="fa-stack fa-sm">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-map-marker fa-stack-1x text-primary"></i>
                            </span>	
                            <?php echo $studio_info[0]['address']; ?><br />
                            
                            <span class="fa-stack">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-mobile fa-stack-1x text-primary"></i>
                            </span>
                            <?php echo $studio_info[0]['contact_no']; ?><br />
                            
                            <span class="fa-stack">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-envelope-square fa-stack-1x text-primary"></i>
                            </span>
                            <?php echo $studio_info[0]['email']; ?><br />
                            
                            <span class="fa-stack">
                              <i class="fa fa-circle fa-stack-2x"></i>
                              <i class="fa fa-globe fa-stack-1x text-primary"></i>
                            </span>
                            <a href="https://<?php echo $studio_info[0]['website']; ?>" target="_blank" class="text-info"><?php echo $studio_info[0]['website']; ?></a><br />
    
                        </span>
                    </p>
                </div>            
            </div>
        </div>
    </section>
    
    <section class="bg-feet">

        <div class="container">
            <div class="row">
            	<div class="col-sm-8 col-sm-offset-2">
                	<div class=" bg-black-trans text-white" style="border-radius: 5px; padding: 25px;">
                        <h2 class="section-heading text-center wow fadeIn" data-wow-delay=".2s">Our Background</h2>
                        <hr class="light wow fadeIn" data-wow-delay=".3s"/>
                        <p class="lead text-center wow fadeIn" data-wow-delay=".4s"><?php echo nl2br($studio_info[0]['background']); ?></p>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    <section class="bg-red">
        <div class="container">
            <div class="row">
            	<div class="col-sm-12 col-lg-8 col-lg-offset-2">
                	<h2 class="section-heading text-center wow fadeIn">Dance Genres</h2>
                    <hr class="light" />
                    <h4 class="text-center">
                        <?php 
							if(count($studio_genres) > 0){
								echo '<div class="row">';
								$sg_count = 1;
								foreach($studio_genres as $r){

									
									$div_class = 'col-xs-6 col-sm-4';
									if(count($studio_genres) == 1){
										$div_class = 'col-sm-4 col-sm-offset-4';
									}
									else if(count($studio_genres) == 2){
										
										$div_class = 'col-sm-4 col-sm-offset-2';
									}
									//reset next div class
									if($sg_count > 1){
										$div_class = 'col-xs-6 col-sm-4';
									}
								?>
									
                                    <div class="<?php echo $div_class; ?> text-center wow fadeIn" data-wow-delay=".<?php echo $sg_count?>s">
                                        <div class="thumbnail text-center" style="padding-top: 30px; padding-bottom: 30px; background-color: transparent">
                                            
                                            <?php echo $this->common->genre_name($r); ?>

                                        </div>
                                    </div>
                                
                                
                                <?php
								$sg_count++;
								}
								echo '</div>';
								
							}
							else{
							 echo '<p class="text-center">No genres.</p>';
							}
						?>
                    
                    
                    </h4>
                </div>
            </div>
        </div>
    </section>
    
    <section class="">
        <div class="container">
            <div class="row">
            	<div class="col-sm-8 col-sm-offset-2">
                	<h2 class="section-heading text-center wow fadeIn" data-wow-delay=".2s">Our Facilities</h2>
                    <hr />
                    <p class="lead text-center wow fadeIn" data-wow-delay=".3s"><?php echo nl2br($studio_info[0]['facilities']); ?></p>
                </div>
            </div>
        </div>
    </section>
    
    <section class="bg-primary">
        <div class="container">
            <div class="row">
            	<div class="col-lg-8 col-lg-offset-2">
                	<h2 class="section-heading text-center wow fadeIn">Our Instructors</h2>
                    <hr class="light" />
                    <div class="row">
					
                    	<?php
						if(count($studio_instructors) > 0){
							
							
							$si_count = 1;
							foreach($studio_instructors as $r=>$value){
								
								$div_class = 'col-sm-4';
								if(count($studio_instructors) == 1){
									$div_class = 'col-sm-4 col-sm-offset-4';
								}
								else if(count($studio_instructors) == 2){
									
									$div_class = 'col-sm-4 col-sm-offset-2';
								}
								//reset next div class
								if($si_count > 1){
									$div_class = 'col-sm-4';
								}
							?>
                                
                             <div class="<?php echo $div_class; ?> text-center wow pulse" data-wow-delay=".<?php echo $si_count?>s" style="padding-bottom: 25px;">
                                        
                                <a href="#" class="portfolio-box text-center view_inst_btn" data-id="<?php echo $value['id']?>">
                                    <img src="<?php echo $this->common->avatar($value['id'], 'instructor') ?>" class="img-circle img-thumbnail" alt="" width="100%">
                                    <div class="portfolio-box-caption div-circle">
                                        <div class="portfolio-box-caption-content">
                                            <div class="project-category text-faded">
                                                View Profile
                                            </div>
                                            <div class="project-name"></div>
                                        </div>
                                    </div>
                                </a>
                                <h4><?php echo $value['name']?></h4>
                            </div>
                        <?php 
							$si_count++;       
							}
						}else{ echo '<h4 class="text-center">No records found.</h4>'; }
						?>
                    
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="login_sectionx" id="classes">

        <div class="container">
                
            
            <div class="row">
            
                <div class="col-md-12">
                	<h2 class="section-heading text-center wow fadeIn">Class Timetable <?php if($_GET['type'] == 'class') { echo 'for '.$this->common->class_name($_GET['id']); }?></h2>
                    <hr />
                
                    <?php if ($this->session->flashdata('ok') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('ok'); ?>
                    </div>
                    <?php } ?>
                
                    <?php if ($this->session->flashdata('error') != ''){ ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>
                
                
                    
                    <div class="panel panel-default">
                        
                        
                        <?php if(count($sorted_schedule) > 0){ ?>
                        <div class="panel-heading hidden">
                            <h4 class="panel-title"><strong>Select your class schedule to book from this studio</strong></h4>
                        </div>
                        <div class="bg-white"><!--bg-white-->
                        <table class="table table-hover table-striped mydataTb">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Class</th>
                                    <th>Available Slots</th>
                                    <th>Instructor(s)</th>
                                    <th>Genre(s)</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($sorted_schedule as $r=>$value){ ?>
                                <tr>
                                    <td title="<?php echo date_format(date_create($value['date']), 'F d, Y - l') ?>"><?php echo $value['date'] ?></td>
                                    <td><?php echo $value['format_start_time'].' - '.$value['format_end_time'] ?></td>
                                    <td><?php echo $this->common->class_name($value['class_id']) ?></td>
                                    <td><?php echo $value['max_student'] - $value['student_count'] ?></td>
                                    <td><?php
										$inst_name = '';
                                        if(count($value['instructors_name']) > 0){
                                            $count = 1;
                                            foreach($value['instructors_name'] as $instructors_name){
                                                $inst_name .= $instructors_name;
                                                if($count < count($value['instructors_name'])){
                                                    $inst_name .= ', ';
                                                }
                                                
                                                $count++;
                                            }
                                        }
                                        else{
                                            $inst_name .= 'Not added';
                                        }
										echo $inst_name;
                                    
                                    ?></td>
                                    <td><?php
										$clss_gnr = '';
                                        if(count($value['genres_id']) > 0){
                                            $count = 1;
                                            foreach($value['genres_id'] as $genres_id){
                                                $clss_gnr .= $this->common->genre_name($genres_id);
                                                if($count < count($value['genres_id'])){
                                                    $clss_gnr .= ', ';
                                                }
                                                $count++;
                                            }
                                        }
                                        else{
                                            $clss_gnr .= 'Not added';
                                        }
										
										echo $clss_gnr;
                                    
                                    ?></td>
                                    <td>
                                    

                                          <button
                                          	class="btn btn-primary btn-outline btn-xs add_class_sched_btn pull-right"
                                            data-id="<?php echo $value['id']?>"
                                            data-cost="<?php echo $this->common->classes_fields($value['class_id'], 'cost') ?>"
                                            data-name="<?php echo $this->common->class_name($value['class_id']) ?>"
                                            data-date="<?php echo $value['date'] ?>"
                                            data-time="<?php echo $value['format_start_time'].' - '.$value['format_end_time'] ?>"
                                            data-classid="<?php echo $value['class_id']; ?>"
                                            data-genre="<?php echo $clss_gnr; ?>"
                                            data-inst="<?php echo $inst_name; ?>">
                                            Book Now
                                          </button>
                                        
                                        
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        </div><!--bg-white-->
                        
                    <?php
                      //loop sorted_schedule
                     } else { echo '<div class="panel-body"><h4 class="text-center text-muted">No class schedules found.</h4></div>';} ?>
                   
                    </div><!--panel-->
                    
                
                    
                    
                </div>
               
            </div>        
        
        
        </div>

        
    </section>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="summaryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h2>Class Booking Summary</h2>
                            <hr class="star-primary">
                  
                            <div class="view_holder"></div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
                                      

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="instructorInfoModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h2>Instructor's Profile</h2>
                            <hr class="star-primary">
                            
                            <div class="instructor_info"></div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
                            