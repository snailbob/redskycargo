<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<ul class="column-list">
<li>AED: United Arab Emirates Dirham</li>
<li>ALL: Albanian Lek</li>
<li>ANG: Netherlands Antillean Gulden</li>
<li>ARS: Argentine Peso*</li>
<li>AUD: Australian Dollar</li>
<li>AWG: Aruban Florin</li>
<li>BBD: Barbadian Dollar</li>
<li>BDT: Bangladeshi Taka</li>
<li>BIF: Burundian Franc</li>
<li>BMD: Bermudian Dollar</li>
<li>BND: Brunei Dollar</li>
<li>BOB: Bolivian Boliviano*</li>
<li>BRL: Brazilian Real*</li>
<li>BSD: Bahamian Dollar</li>
<li>BWP: Botswana Pula</li>
<li>BZD: Belize Dollar</li>
<li>CAD: Canadian Dollar</li>
<li>CHF: Swiss Franc</li>
<li>CLP: Chilean Peso*</li>
<li>CNY: Chinese Renminbi Yuan</li>
<li>COP: Colombian Peso*</li>
<li>CRC: Costa Rican Colón*</li>
<li>CVE: Cape Verdean Escudo*</li>
<li>CZK: Czech Koruna*</li>
<li>DJF: Djiboutian Franc*</li>
<li>DKK: Danish Krone</li>
<li>DOP: Dominican Peso</li>
<li>DZD: Algerian Dinar</li>
<li>EGP: Egyptian Pound</li>
<li>ETB: Ethiopian Birr</li>
<li>EUR: Euro</li>
<li>FJD: Fijian Dollar</li>
<li>FKP: Falkland Islands Pound*</li>
<li>GBP: British Pound</li>
<li>GIP: Gibraltar Pound</li>
<li>GMD: Gambian Dalasi</li>
<li>GNF: Guinean Franc*</li>
<li>GTQ: Guatemalan Quetzal*</li>
<li>GYD: Guyanese Dollar</li>
<li>HKD: Hong Kong Dollar</li>
<li>HNL: Honduran Lempira*</li>
<li>HRK: Croatian Kuna</li>
<li>HTG: Haitian Gourde</li>
<li>HUF: Hungarian Forint*</li>
<li>IDR: Indonesian Rupiah</li>
<li>ILS: Israeli New Sheqel</li>
<li>INR: Indian Rupee*</li>
<li>ISK: Icelandic Króna</li>
<li>JMD: Jamaican Dollar</li>
<li>JPY: Japanese Yen</li>
<li>KES: Kenyan Shilling</li>
<li>KHR: Cambodian Riel</li>
<li>KMF: Comorian Franc</li>
<li>KRW: South Korean Won</li>
<li>KYD: Cayman Islands Dollar</li>
<li>KZT: Kazakhstani Tenge</li>
<li>LAK: Lao Kip*</li>
<li>LBP: Lebanese Pound</li>
<li>LKR: Sri Lankan Rupee</li>
<li>LRD: Liberian Dollar</li>
<li>MAD: Moroccan Dirham</li>
<li>MDL: Moldovan Leu</li>
<li>MNT: Mongolian Tögrög</li>
<li>MOP: Macanese Pataca</li>
<li>MRO: Mauritanian Ouguiya</li>
<li>MUR: Mauritian Rupee*</li>
<li>MVR: Maldivian Rufiyaa</li>
<li>MWK: Malawian Kwacha</li>
<li>MXN: Mexican Peso*</li>
<li>MYR: Malaysian Ringgit</li>
<li>NAD: Namibian Dollar</li>
<li>NGN: Nigerian Naira</li>
<li>NIO: Nicaraguan Córdoba*</li>
<li>NOK: Norwegian Krone</li>
<li>NPR: Nepalese Rupee</li>
<li>NZD: New Zealand Dollar</li>
<li>PAB: Panamanian Balboa*</li>
<li>PEN: Peruvian Nuevo Sol*</li>
<li>PGK: Papua New Guinean Kina</li>
<li>PHP: Philippine Peso</li>
<li>PKR: Pakistani Rupee</li>
<li>PLN: Polish Złoty</li>
<li>PYG: Paraguayan Guaraná*</li>
<li>QAR: Qatari Riyal</li>
<li>RUB: Russian Ruble</li>
<li>SAR: Saudi Riyal</li>
<li>SBD: Solomon Islands Dollar</li>
<li>SCR: Seychellois Rupee</li>
<li>SEK: Swedish Krona</li>
<li>SGD: Singapore Dollar</li>
<li>SHP: Saint Helenian Pound*</li>
<li>SLL: Sierra Leonean Leone</li>
<li>SOS: Somali Shilling</li>
<li>STD: São Tomé and Príncipe Dobra</li>
<li>SVC: Salvadoran Colón*</li>
<li>SZL: Swazi Lilangeni</li>
<li>THB: Thai Baht</li>
<li>TOP: Tongan Paʻanga</li>
<li>TTD: Trinidad and Tobago Dollar</li>
<li>TWD: New Taiwan Dollar</li>
<li>TZS: Tanzanian Shilling</li>
<li>UAH: Ukrainian Hryvnia</li>
<li>UGX: Ugandan Shilling</li>
<li>USD: United States Dollar</li>
<li>UYU: Uruguayan Peso*</li>
<li>UZS: Uzbekistani Som</li>
<li>VND: Vietnamese Đồng</li>
<li>VUV: Vanuatu Vatu</li>
<li>WST: Samoan Tala</li>
<li>XAF: Central African Cfa Franc</li>
<li>XOF: West African Cfa Franc*</li>
<li>XPF: Cfp Franc*</li>
<li>YER: Yemeni Rial</li>
<li>ZAR: South African Rand</li>
</ul>

<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
<script>
//	var arr = [];
//	var base_url = 'http://localhost/nguyen/transit/';
//	$('ul').find('li').each(function(index, element) {
//        arr.push($(this).html());
//    });
//	console.log(arr);
//	$.post(
//		base_url+'formsubmits/currs',
//		{arr: arr},
//		function(res){
//			console.log(res);
//		},
//		'json'
//	).error(function(err){
//		console.log(err);
//	});
</script>
</body>
</html>